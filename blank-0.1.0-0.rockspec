package = "blank"
version = "0.1.0-0"

description = {
  summary = "XXX.",
  detailed = [[
    XXX.
  ]],
  license = "",
  homepage = "http://XXX",
  maintainer = "Developer Name <email>"
}

source = {
  url = ""
}

dependencies = {
  "lua >= 5.1, <= 5.3",
  "justo >= 1.0.alpha1",
  "justo-assert >= 1.0.alpha1",
  "justo-cli >= 1.0.alpha1",
  "justo-plugin-cli >= 1.0.alpha1"
}

build = {
  type = "builtin",

  modules = {
    ["xxx.Justo"] = "Justo.lua"
  }
}
